import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-lesson11',
  templateUrl: './lesson11.component.html',
  styleUrls: ['./lesson11.component.scss']
})
export class Lesson11Component implements OnInit {
  userName = '';
  response: any;

  constructor(private http: HttpClient) {
  }
  search() {
    this.http.get('https://api.github.com/users/' + this.userName)
      .subscribe((response) => {
        this.response = response;
        console.log(this.response);
      });
  }

  ngOnInit() {
  }

}
