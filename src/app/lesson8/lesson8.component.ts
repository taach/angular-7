import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson8',
  templateUrl: './lesson8.component.html',
  styleUrls: ['./lesson8.component.scss']
})
export class Lesson8Component implements OnInit {

  /*lesson8 ngClass ngStyle*/
  visibility: boolean = true;

  constructor() {
  }

  hide() {
    this.visibility = !this.visibility;
  }

  ngOnInit() {
  }

}
