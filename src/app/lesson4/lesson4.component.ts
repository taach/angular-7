import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson4',
  templateUrl: './lesson4.component.html',
  styleUrls: ['./lesson4.component.scss']
})
export class Lesson4Component implements OnInit {

  /*lesson4 - ngFor lesson*/
  items = ['Angular', 'React', 'Vue', 'Bootstrap', 'Node.js'];

  constructor() { }

  ngOnInit() {
  }

}
