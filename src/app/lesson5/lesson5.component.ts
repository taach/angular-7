import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson5',
  templateUrl: './lesson5.component.html',
  styleUrls: ['./lesson5.component.scss']
})
export class Lesson5Component implements OnInit {

  /*lesson5 - ngIf lesson*/
  isLogged = true;

  constructor() { }

  ngOnInit() {
  }

}
