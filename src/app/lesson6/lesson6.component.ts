import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson6',
  templateUrl: './lesson6.component.html',
  styleUrls: ['./lesson6.component.scss']
})
export class Lesson6Component implements OnInit {

  /*lesson6 - events lesson*/
  isCollapsed: boolean = true;

  constructor() { }

  /*lesson6 - events lesson*/
  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }


  ngOnInit() {
  }

}
